
#include <Windows.h>
#include <Psapi.h>

#include "RedoBlHooks.hpp"

bool init(){
	BlInit;
	
	ADDR BlScanHex(addrBringWindowToForeground, "6A 03 6A 00 6A 00 68 ? ? ? ? FF 15 ? ? ? ? 6A 01 FF 35 ? ? ? ? FF 15 ? ? ? ? FF 35 ? ? ? ? FF 15 ? ? ? ? 6A 03 6A 00 68 ? ? ? ? 68 ? ? ? ? FF 15 ? ? ? ? C3");
	
	int dataSetForegroundWindow = *(ADDR*)(addrBringWindowToForeground+0x27);
	
	BYTE pattSetForegroundWindowCall[12] = {0,0,0,0,0,0, 0xFF, 0x15, 0,0,0,0};
	*(int*)(pattSetForegroundWindowCall+8) = dataSetForegroundWindow;
	
	BlPrintf("NoFocus: SetForegroundWindow data = %08x", dataSetForegroundWindow);
	BlPrintf("NoFocus: SetForegroundWindow call pattern = %02x %02x %02x %02x %02x %02x",
		(int)pattSetForegroundWindowCall[6],
		(int)pattSetForegroundWindowCall[7],
		(int)pattSetForegroundWindowCall[8],
		(int)pattSetForegroundWindowCall[9],
		(int)pattSetForegroundWindowCall[10],
		(int)pattSetForegroundWindowCall[11]
	);
	
	int callsdisabled = BlPatchAllMatches(12, pattSetForegroundWindowCall, "??????xxxxxx", "\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90");
	
	BlPrintf("NoFocus: Disabled %i calls to SetForegroundWindow.", callsdisabled);
	
	return true;
}

int __stdcall DllMain( HINSTANCE hInstance, unsigned long reason, void *reserved ){
	switch(reason){
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return true;
		default:
			return true;
	}
}
