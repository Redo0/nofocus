
Introducing NoFocus: a simple DLL to stop Blockland from stealing the focus a billion times while loading.

Stops the Blockland window from grabbing the focus by disabling all calls to SetForegroundWindow.
It completely disables the game's ability to force itself to the foreground, so it won't happen when you get kicked from a server either.

Technical description:
It works by overwriting all calls to SetForegroundWindow (A Windows API function in User32.dll) and their argument stack pushes with NOPs.
It finds the calls by scanning for the pattern 00 FF 15 XX XX XX XX / ?xxxxxx and overwriting all occurrences with 90 90 90 90 90 90 90, where XX XX XX XX is the address of the pointer to SetForegroundWindow.
The first 00 / ? is the stack push of the only argument; it's either 50 or 51 for pushing eax or ecx respectively.
The FF 15 is the opcode for a 32-bit near call to a memory address, that address (the next 4 bytes) being a pointer to SetForegroundWindow.
By replacing all of these bytes with NOPs, it prevents the game from calling the function, while also preserving the stack.
The values of XX XX XX XX are found by scanning for the torque function BringWindowToFront, which contains one such call to SetForegroundWindow, and reading the contents of that call.
You could probably make this work with other 32-bit applications just by finding a different function that calls SetForegroundWindow, or discovering and entering its address manually.
